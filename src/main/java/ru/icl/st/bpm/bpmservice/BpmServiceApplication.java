package ru.icl.st.bpm.bpmservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BpmServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BpmServiceApplication.class, args);
	}
}
